# Start from the rootproject/root:6.26.10-conda
FROM rootproject/root:6.26.10-conda

# Put the current repo (the one in which this Dockerfile resides) in the /analysis/skim directory
# Note that this directory is created on the fly and does not need to reside in the repo already
COPY . /analysis/skim 

# Make /analysis/skim the default working directory 
WORKDIR /analysis/skim

# Compile an executable named 'skim' from the skim.cxx source file
RUN echo " >>> Compile skimming executable ..." && \
    COMPILER=$(root-config --cxx) && \
    FLAGS=$(root-config --cflags --libs) && \
    $COMPILER -g -std=c++17 -O3 -Wall -Wextra -Wpedantic -o skim skim.cxx $FLAGS
